# chef

chef is elixi.re's helper discord bot.

## usage

copy `config.example.yml` to `config.yml` and fill it out with the relevant
details.

install python dependencies:

```sh
$ pip install -U discord.py
$ pip install -r docker-requirements.txt

# install uvloop for more speed (optional)
$ pip install uvloop
```

then boot chef by launching the lifesaver cli:

```sh
$ python -m lifesaver.cli
```
