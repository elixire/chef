CREATE TYPE job_state AS ENUM (
  'waiting',  -- waiting for user to join server
  'pending',  -- waiting for admin approval
  'finished', -- a decision was reached
  'cancelled' -- cancelled by an admin
);

-- jobs created by incoming registrations
CREATE TABLE jobs (
  id SERIAL PRIMARY KEY,
  message_id BIGINT NOT NULL,
  state job_state NOT NULL,

  user_id BIGINT NOT NULL,
  username TEXT NOT NULL,
  email TEXT NOT NULL,

  discord_tag TEXT NOT NULL,
  discord_user_id BIGINT,

  outcome BOOLEAN,
  admin_discord_id BIGINT
);

-- manual links between discord accounts and elixire users
CREATE TABLE manual_links (
  discord_user_id BIGINT PRIMARY KEY,
  elixire_user_id BIGINT UNIQUE
);
