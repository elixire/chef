__all__ = ['JobSearch']

from typing import Optional

import discord
from discord.ext import commands

from chef.elixire import ElixireUser, ElixireError
from chef.exts.approval.job import Job


class LinkedAccounts:
    def __init__(self, account, member):
        #: The elixi.re user.
        self.account: ElixireUser = account

        #: The Discord member.
        self.member: discord.Member = member

    @classmethod
    async def resolve_account_id(cls, bot, member: discord.Member) -> Optional[int]:
        """Attempt to resolve an elixi.re account ID for a Discord member."""
        async with bot.pool.acquire() as conn:
            row = await conn.fetchrow("""
                SELECT user_id
                FROM jobs
                WHERE discord_user_id = $1
                AND state = 'finished'
                AND outcome = TRUE
            """, member.id)

            manual_row = await conn.fetchrow("""
                SELECT elixire_user_id
                FROM manual_links
                WHERE discord_user_id = $1
            """, member.id)

        if not row and not manual_row:
            return None

        if manual_row:
            return manual_row['elixire_user_id']
        if row:
            return row['user_id']

    @classmethod
    async def resolve_member_info(cls, bot, account: ElixireUser) -> Optional[tuple]:
        """Attempt to resolve a Discord user ID for an elixi.re account.

        Returns a Discord user ID and DiscordTag (might be None) in a tuple.
        """
        async with bot.pool.acquire() as conn:
            row = await conn.fetchrow("""
                SELECT discord_user_id, discord_tag
                FROM jobs
                WHERE user_id = $1
            """, account.id)

            manual_row = await conn.fetchrow("""
                SELECT discord_user_id
                FROM manual_links
                WHERE elixire_user_id = $1
            """, account.id)

        if not row and not manual_row:
            return None

        if manual_row:
            return manual_row['discord_user_id'], None
        if row:
            return row['discord_user_id'], row['discord_tag']

    @classmethod
    async def resolve_account(cls, bot, member: discord.Member) -> Optional[ElixireUser]:
        """Attempt to resolve an elixi.re account for a Discord member."""
        user_id = await cls.resolve_account_id(bot, member)

        if not user_id:
            return None

        try:
            account = await bot.api.fetch_user(user_id)
            return account
        except ElixireError:
            return None

    @classmethod
    async def resolve_member(cls, bot, account: ElixireUser) -> Optional[discord.Member]:
        """Attempt to resolve a Discord member for an elixi.re account.

        If the member couldn't be found, ``None`` is returned.
        """
        info = await cls.resolve_member_info(bot, account)

        if not info:
            return None

        member_id, member_tag = info

        if member_id:
            member = bot.elixire.get_member(member_id)
            if member:
                return member

        member = bot.elixire.get_member_named(member_tag)
        if member:
            return member

        return None

    @classmethod
    async def convert(cls, ctx, arg):
        # attempt to convert member reference
        try:
            member = await commands.MemberConverter().convert(ctx, arg)
            account = await cls.resolve_account(ctx.bot, member)
            return cls(account, member)
        except commands.BadArgument:
            pass

        # attempt to convert elixire user reference
        try:
            account = await ElixireUser.convert(ctx, arg)
            member = account._associated_member or await cls.resolve_member(ctx.bot, account)
            return cls(account, member)
        except commands.BadArgument:
            pass

        raise commands.BadArgument('Unable to resolve an elixi.re user or a '
                                   'Discord user.')

    @property
    def good(self):
        return self.account and self.member


class JobSearch(commands.Converter):
    async def convert(self, ctx, arg):
        async with ctx.pool.acquire() as conn:
            records = await conn.fetch("""
                SELECT * FROM jobs
                WHERE to_tsvector(jobs::text) @@ to_tsquery($1);
            """, arg)

        jobs = [Job(ctx.cog, record) for record in records]
        return jobs
