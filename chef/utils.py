__all__ = ['timestamp']

import datetime


def timestamp(dt=datetime.datetime.utcnow()) -> str:
    return dt.strftime('%Y-%m-%d %H:%M:%S') + ' UTC'
