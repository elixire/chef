__all__ = ['ChefError', 'JobCreationError']


class ChefError(Exception):
    pass


class JobCreationError(ChefError):
    pass
