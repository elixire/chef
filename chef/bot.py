import discord
import lifesaver
from discord.ext import commands
from discord import Intents

from chef.elixire import Elixire


class Chef(lifesaver.Bot):
    def __init__(self, *args, **kwargs):
        intents = Intents.default()

        # members intent is required to resolve discord.User/discord.Member
        # on command parameters
        intents.members = True
        intents.typing = False

        super().__init__(*args, intents=intents, **kwargs)
        self.ignored_errors = [commands.CheckFailure, commands.NotOwner]
        self.api = Elixire(self)

    async def close(self):
        await self.api.close()
        await super().close()

    @property
    def elixire(self):
        return self.get_guild(self.config.elixire.guild_id)

    @property
    def has_account_role(self):
        if self.elixire is None:
            return None
        return self.elixire.get_role(self.config.elixire.has_account_role_id)

    def ticks(self, *, ids: bool = False):
        if ids:
            return (self.tick().id, self.tick(False).id)
        return (self.tick(), self.tick(False))

    async def is_owner(self, user):
        return self.is_dev(user) or await super().is_owner(user)

    def is_dev(self, member) -> bool:
        if self.elixire is None:
            return False

        if isinstance(member, discord.User):
            # not in a guild, so let's fetch the member from elixi.re's guild
            member = self.elixire.get_member(member.id)
            if member is None:
                return False

        allowed_roles = self.config.restrict.allowed_roles
        return any(role.id in allowed_roles for role in member.roles)
