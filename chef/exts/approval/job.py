__all__ = ['Job']

import logging
from typing import Optional, Union

import discord
from discord.ext import commands
from discord.utils import snowflake_time
from lifesaver.utils.formatting import human_delta

from chef.utils import timestamp
from chef.constants import BAD_PROVIDERS

log = logging.getLogger(__name__)


class Job:
    def __init__(self, cog, record):
        self.cog = cog
        self.record = record
        self.message = None

    @property
    def bot(self):
        return self.cog.bot

    @property
    def guild(self) -> discord.Guild:
        return self.bot.elixire

    @property
    def channel(self) -> discord.TextChannel:
        return self.cog.user_registers

    @property
    def pool(self):
        return self.cog.pool

    @property
    def created_at(self):
        return snowflake_time(self.user_id)

    @property
    def discord_user(self) -> Optional[discord.Member]:
        """Resolves the Discord member associated with this job, using the data
        that is currently associated with this job.

        When the job is first created, we only have the user's DiscordTag. After
        the user joins and the job is pushed to the "pending" state, we have the
        user's Discord user ID. The Discord user ID is tried first, if we have
        it. The DiscordTag is used as a fallback.
        """
        tag = self.discord_tag
        user_id = self.discord_user_id

        if user_id:
            return self.guild.get_member(user_id)

        return self.guild.get_member_named(tag)

    async def fetch_discord_user(self) -> Optional[Union[discord.User, discord.Member]]:
        """Resolves the Discord member associated with this job, falling back
        to ``bot.fetch_user`` if the user can no longer be found in the server.
        """
        if self.discord_user:
            return self.discord_user

        user_id = self.discord_user_id

        if not user_id:
            return None

        return await self.bot.fetch_user(user_id)

    @property
    def embed(self):
        embed = discord.Embed(title=f'Job #{self.id}')

        if self.discord_user_id is not None:
            discord_user_id = self.discord_user_id
            discord_info = f'Discord: <@{discord_user_id}> (`{discord_user_id}`, {self.discord_tag})'
        else:
            discord_info = f'Discord: {self.discord_tag}'

        embed.description = (
            f'{discord_info}\n'
            f'elixi.re: {self.username}/{self.email} (`{self.user_id}`)\n\n'
            f'Created elixi.re account {human_delta(self.created_at)} ago'
        )

        embed.add_field(name='Jump to message', value=self.message.jump_url)

        if self.outcome in (False, True) or self.state == 'cancelled':
            admin = self.bot.get_user(self.admin_discord_id)

            if self.state == 'cancelled':
                embed.colour = discord.Color.orange()
                embed.set_footer(text=f'Cancelled by {admin}', icon_url=admin.avatar_url)
            else:
                if self.outcome:
                    embed.colour = discord.Color.green()
                    embed.set_footer(text=f'Approved by {admin}', icon_url=admin.avatar_url)
                else:
                    embed.colour = discord.Color.red()
                    embed.set_footer(text=f'Denied by {admin}', icon_url=admin.avatar_url)
        else:
            if self.state == 'pending':
                embed.colour = discord.Color.gold()
                embed.set_footer(text='Waiting for a decision...')
            elif self.state == 'waiting':
                embed.set_footer(text='Waiting for user to join...')

        return embed

    @classmethod
    async def convert(cls, ctx, job_id: str):
        try:
            job_id = int(job_id)
        except ValueError:
            raise commands.BadArgument('Invalid job ID.')

        try:
            job = await ctx.cog.fetch_job_by_id(job_id)
        except Exception as error:
            raise commands.BadArgument(f'Unable to fetch job {job_id}: {error}')

        if not job:
            raise commands.BadArgument('Job not found.')

        return job

    @classmethod
    async def from_record(cls, record, *, cog, fetch: bool = True):
        job = cls(cog, record)
        if fetch:
            await job.fetch_message()
        return job

    async def _add_reactions(self, message=None):
        message = message or self.message
        for tick in self.bot.ticks():
            await message.add_reaction(tick)

    async def _clear_reactions(self, message=None):
        message = message or self.message
        try:
            await message.clear_reactions()
        except discord.HTTPException:
            pass

    async def fetch_message(self):
        self.message = await self.channel.fetch_message(self.message_id)

    async def add_has_account_role(self):
        """Adds the "has account" role to the Discord member associated with
        this job.

        Sends error messages to the #user_registers channel as appropriate.
        """
        user = self.discord_user
        role = self.bot.has_account_role

        error_prefix = f'{self.bot.tick(False)} Unable to give role to {self.discord_tag}.'

        if not user:
            await self.channel.send(f'{error_prefix} User not found.')
            return

        if not role:
            await self.channel.send(f'{error_prefix} Unable to find role.')
            return

        try:
            await user.add_roles(role)
        except discord.HTTPException as error:
            await self.channel.send(f'{error_prefix} Error: `{error}`')

    async def prompt_ditch_denial(self, *, old_member: discord.Member):
        """Send a message prompting for the denial of this job due to the user
        leaving prematurely.
        """
        originally_joined = human_delta(old_member.joined_at)
        registered_at = human_delta(self.message.created_at)

        embed = discord.Embed(
            title=f'\N{outbox tray} {old_member} has left',
            description=(
                'This user has left while their account was still waiting to be reviewed. '
                f'Press {self.bot.tick()} to irreversibly delete their account.'
                '\n\n**Note:** The user may have rejoined since then. Please '
                'double check before deleting.'),
            color=discord.Color.red())
        embed.add_field(
            name='Information', value=(
                f'Originally joined server {originally_joined} ago\n'
                f'Created Discord account {registered_at} ago'))
        embed.add_field(
            name='Original Job', value=f'#{self.id}, [Jump]({self.message.jump_url})')
        embed.set_footer(
            text="If these buttons don't work, use the buttons on the original job message.")

        msg = await self.channel.send(embed=embed)
        await self._add_reactions(msg)

        # keep track of the ditch prompt. if the user rejoins, we can delete this
        # message.
        self.cog._ditch_prompts[old_member.id] = msg

        # probably unnecessary to create a task here, but why not?
        self.bot.loop.create_task(self._denial_waiter(msg, original_embed=embed))

    async def _denial_waiter(self, msg: discord.Message, *, original_embed):
        def check(reaction, user):
            return (
                reaction.message.id == msg.id  # Message.__eq__, where art thou?
                and reaction.emoji in self.bot.ticks()
                and self.bot.is_dev(user)
            )

        reaction, user = await self.bot.wait_for('reaction_add', check=check)
        is_denying = reaction.emoji == self.bot.tick()

        new_embed = original_embed.copy()
        new_embed.description = 'This user has left while their account was still waiting to be reviewed.'
        new_embed.set_footer(text=discord.Embed.Empty)
        await msg.edit(embed=new_embed)

        if not is_denying:
            await self.channel.send(
                f"{self.bot.tick()} Okay, keeping {self.discord_user}'s account.")
            await self._clear_reactions(msg)
            return

        # deny!
        await self._clear_reactions(msg)
        await self.finish(user, outcome=False)

    async def cancel(self, canceller: discord.User):
        message = f'{self.cog.messages.job_cancelled} by {canceller} (`{canceller.id}`).'

        await self._clear_reactions()
        await self.message.edit(content=message, embed=None)

        async with self.pool.acquire() as conn:
            await conn.execute("""
                UPDATE jobs SET admin_discord_id = $2
                WHERE id = $1
            """, self.id, canceller.id)

        await self.set_state('cancelled')

    async def finish(self, executor: discord.User, *, outcome: bool):
        """Finishes this job.

        This method will either send an approval email or delete the user,
        depending on the outcome. The outcome and the admin who made the
        decision is stored in the database.

        This also sets the job's state to "finished".
        """
        await self._clear_reactions()

        log.info('%d: Finishing job: outcome was %s.', self.id, outcome)

        outcome_string = 'approved' if outcome else 'denied'

        content = (
            f"{self.bot.tick(outcome)} {self.discord_tag} was **{outcome_string}** "
            f"by {executor} (`{executor.id}`)."
        )

        await self.message.edit(
            content=content + f'\n\n{timestamp()}', embed=None)

        # echo the message again just in case the job message is far back.
        await self.channel.send(content)

        # send the email for approval
        async with self.channel.typing():
            if outcome:
                try:
                    await self.bot.api.send_email(self.user_id)
                except Exception as error:
                    message = f'{self.bot.tick(False)} Failed to send email: `{error}`'
                else:
                    message = f'{self.bot.tick()} An activation email was sent to {self.discord_tag}.'

                await self.channel.send(message)
                await self.add_has_account_role()
            else:
                try:
                    await self.bot.api.delete_user(self.user_id)
                except Exception as error:
                    message = f'{self.bot.tick(False)} Failed to delete user: `{error}`'
                else:
                    message = f"{self.bot.tick()} {self.discord_tag}'s elixi.re account was deleted."
                await self.channel.send(message)

        async with self.pool.acquire() as conn:
            await conn.execute("""
                UPDATE jobs SET outcome = $2, admin_discord_id = $3
                WHERE id = $1
            """, self.id, outcome, executor.id)

        await self.set_state('finished')

    async def push_to_pending(self, user):
        log.debug('%d: moving to pending...', self.id)
        await self.set_state('pending')
        async with self.pool.acquire() as conn:
            await conn.execute("""
                UPDATE jobs SET discord_user_id = $2
                WHERE id = $1
            """, self.id, user.id)

        embed = discord.Embed()
        embed.color = discord.Color.green()
        embed.description = self.cog.messages.job_pending

        email_chunks = self.email.split('@')
        if len(email_chunks) > 1 and email_chunks[1] in BAD_PROVIDERS:
            embed.color = discord.Color.red()
            embed.description += f'\n\n\N{WARNING SIGN} **User uses a bad email provider, be careful!**'

        embed.add_field(
            name='Discord',
            value=(f'{user.mention} (`{user.id}`, {user.name})'
                   f'\nJoined Discord {human_delta(user.created_at)} ago')
        )
        embed.add_field(
            name='elixi.re',
            value=(f'{self.username} (`{self.user_id}`)'
                   f'\nRegistered {human_delta(self.created_at)} ago')
        )
        embed.add_field(name='Joined server', value=f'{user.joined_at} UTC')
        embed.set_footer(text=f'Job #{self.id}')

        log.debug('%d: editing message to reflect new pending status', self.id)
        await self.message.edit(content='', embed=embed)
        await self._add_reactions()

    async def set_state(self, new_state: str):
        log.debug('%d: moving to -> %s', self.id, new_state)

        async with self.pool.acquire() as conn:
            await conn.execute("""
                UPDATE jobs SET state = $2
                WHERE id = $1
             """, self.id, new_state)

    def __eq__(self, other):
        return other.id == self.id

    def __getattr__(self, name):
        try:
            return self.record[name]
        except KeyError:
            raise AttributeError(name)

    def __str__(self):
        if self.outcome is False:
            outcome = 'was denied'
        elif self.outcome is True:
            outcome = 'was approved'
        else:
            outcome = 'n/a'

        base_information = f'#{self.id} ({self.state}, {outcome}): {self.username} ({self.email})/'

        if self.discord_user_id:
            base_information += f'<@{self.discord_user_id}> ({self.discord_user_id}, {self.discord_tag})'
        else:
            base_information += self.discord_tag

        return base_information

    def __repr__(self):
        return f'<Job id={self.id} user_id={self.user_id} state={self.state}>'
