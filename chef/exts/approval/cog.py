import logging
from typing import Dict, Optional

import asyncio
import asyncpg
import discord
import lifesaver
from discord.ext import commands
from discord.utils import get
from jishaku.paginators import PaginatorEmbedInterface
from lifesaver.utils.formatting import pluralize, human_delta

from chef.errors import JobCreationError
from chef.elixire import ElixireError, ElixireUser
from chef.converters import JobSearch
from .job import Job

log = logging.getLogger(__name__)


class Approval(lifesaver.Cog):
    def __init__(self, bot):
        super().__init__(bot)

        self.headers = {
            'Authorization': self.bot.config.elixire.admin_token,
        }

        #: A dictionary of member IDs to ditch prompts.
        #:
        #: Keeping track of these is useful so we can delete them if the user
        #: eventually rejoins. However, this doesn't persist.
        self._ditch_prompts: Dict[int, discord.Message] = {}

    @property
    def user_registers(self):
        return self.bot.get_channel(
            self.bot.config.elixire.user_registers_channel_id)

    @property
    def messages(self):
        return self.bot.config.messages

    async def request(self, method, endpoint, **kwargs):
        url = self.bot.config.elixire.domain + endpoint
        async with self.session.request(method, url, headers=self.headers, **kwargs) as resp:
            resp.raise_for_status()
            return await resp.json()

    async def fetch_job_by_id(self, job_id: int) -> Optional[Job]:
        """Fetches a job by ID."""
        async with self.pool.acquire() as conn:
            try:
                record = await conn.fetchrow("""
                    SELECT * FROM jobs
                    WHERE id = $1
                """, job_id)
            except asyncpg.exceptions.DataError:
                return None

            if not record:
                return None

            return await Job.from_record(record, cog=self)

    async def create_job(self, embed: discord.Embed):
        """Create a job from an embed created by the user registration webhook.

        This method extracts information from the embed that is used to create
        the job.
        """
        fields = embed.fields

        if embed.title != 'user registration webhook':
            raise JobCreationError(f'Unexpected webhook title: {embed.title}')

        def get_field(name: str):
            """Return a field from the embed. Raises if it doesn't exist."""
            field = get(fields, name=name)

            if not field:
                raise JobCreationError(f'Failed to find required field for job creation: {name}')

            return field.value

        user_id = get_field('userid')

        try:
            user_id = int(user_id)
        except ValueError:
            raise JobCreationError(f'Failed to convert user ID to integer ({user_id})')

        username = get_field('user name')
        email = get_field('email')
        discord_tag = get_field('discord user')

        # check if there is already a job for this user
        async with self.bot.pool.acquire() as conn:
            job_record = await conn.fetchrow("""
                SELECT * FROM jobs
                WHERE discord_tag = $1 AND (state = 'waiting' OR state = 'pending')
            """, discord_tag)

            if job_record is not None:
                await self.user_registers.send(
                    f'{self.bot.tick(False)} There is already a non-finished job for {discord_tag}.'
                )
                return

        waiting_message = self.messages.job_waiting
        message = await self.user_registers.send(f'{waiting_message} (Job ID: ...)')

        async with self.bot.pool.acquire() as conn:
            record = await conn.fetchrow("""
                INSERT INTO jobs (message_id, state, user_id, username, email, discord_tag)
                VALUES ($1, 'waiting', $2, $3, $4, $5) RETURNING id;
            """, message.id, user_id, username, email, discord_tag)

        await message.edit(content=f'{waiting_message} (Job ID: {record["id"]})')

        # push the job to pending immediately if the user is already here
        member = discord.utils.find(lambda member: str(member) == discord_tag, self.bot.elixire.members)
        if member:
            await self.check_member(member)

    async def check_member(self, member: discord.Member):
        """Checks if a job exists for a member, and pushes the job to pending if necessary."""

        async with self.pool.acquire() as conn:
            job_record = await conn.fetchrow("""
                SELECT * FROM jobs
                WHERE discord_tag = $1 AND state = 'waiting'
            """, str(member))

            if job_record:
                log.debug('Pushing job %d to pending state due to %s',
                          job_record['id'], member.id)
                job = await Job.from_record(job_record, cog=self)
                await job.push_to_pending(member)

    @lifesaver.command(typing=True)
    async def todo(self, ctx):
        """Views jobs that you need to deal with."""
        async with ctx.pool.acquire() as conn:
            pending_jobs = await conn.fetch("""
                SELECT *
                FROM jobs
                WHERE state = 'pending'
                ORDER BY user_id ASC
            """)

        if not pending_jobs:
            await ctx.send('\N{SHOOTING STAR} There are no pending jobs.')
            return

        paginator = commands.Paginator(prefix='', suffix='', max_size=2048)
        interface = PaginatorEmbedInterface(ctx.bot, paginator, owner=ctx.author)

        accounts = pluralize(account=len(pending_jobs), with_quantity=True, with_indicative=True)
        paginator.add_line(f'{accounts} waiting to be confirmed:\n')

        for record in pending_jobs:
            # create job structure from record for easy field access
            # (message not fetched here)
            job = Job(self, record)

            jump_url = ('https://discordapp.com/channels/'
                        f'{self.bot.elixire.id}/{self.user_registers.id}/{job.message_id}')
            age = human_delta(job.created_at)
            info = f'{job.discord_tag} ({job.username}, {job.email}) ({age} ago)'

            paginator.add_line(f'**#{job.id}:** [{info}]({jump_url})')

        await interface.send_to(ctx)

    @lifesaver.command()
    async def delete(self, ctx, *, user: ElixireUser):
        """Deletes an elixi.re user."""
        if user.admin:
            await ctx.send(f"{ctx.tick(False)} Can't delete {user}, they are an admin.")
            return

        if not await ctx.confirm('Delete this user?', f'Elixi.re user {user} will be deleted forever.'):
            await ctx.send('Deletion cancelled.')
            return

        try:
            progress = await ctx.send(f'{ctx.emoji("loading")} Deleting {user}...')
            await ctx.bot.api.delete_user(user.id)
        except ElixireError as err:
            await progress.edit(content=f'{ctx.tick(False)} Failed to delete: {err}')
        else:
            await progress.edit(content=f'{ctx.tick()} Deleted elixi.re user {user}.')

    @lifesaver.command(typing=True, aliases=['status', 'show'])
    async def info(self, ctx, *, job: Job):
        """Views information about a job."""
        await ctx.send(embed=job.embed)

    @lifesaver.command(typing=True)
    async def search(self, ctx, *, query: JobSearch):
        """Searches for jobs."""
        ctx.new_paginator(prefix='', suffix='')

        if query:
            for job in query[:10]:
                ctx += f'\N{BULLET} {job}'
        else:
            ctx += 'No results.'

        await ctx.send_pages()

    @lifesaver.command(typing=True)
    async def cancel(self, ctx, *jobs: Job):
        """Cancels jobs."""
        ctx.new_paginator(prefix='', suffix='')

        for job in jobs:
            if job.state not in ('pending', 'waiting'):
                ctx += f"{ctx.tick(False)} Can't cancel #{job.id} -- it is {job.state}."
                continue
            await job.cancel(ctx.author)
            ctx += f'{ctx.tick()} Cancelled #{job.id}.'

        await ctx.send_pages()

    @lifesaver.command()
    async def clear_waiting(self, ctx):
        """Deletes all waiting jobs.

        Deletes all jobs that are still waiting for a user.
        """

        async with ctx.pool.acquire() as conn:
            n_waiting_accounts = await conn.fetchval("""
                SELECT COUNT(*)
                FROM jobs
                WHERE state = 'waiting'
            """)

        if n_waiting_accounts == 0:
            await ctx.send('There are no waiting jobs.')
            return

        num = pluralize(account=n_waiting_accounts)
        if not await ctx.confirm('Are you sure?', f'{num} will be deleted.'):
            return

        async with ctx.typing():
            async with ctx.pool.acquire() as conn:
                await conn.execute("""
                    DELETE FROM jobs
                    WHERE state = 'waiting'
                """)

        await ctx.send(f'{ctx.tick()} Deleted {num}.')

    @lifesaver.command(typing=True)
    async def deny(self, ctx, *jobs: Job):
        """Denies jobs."""
        ctx.new_paginator(prefix='', suffix='')

        for job in jobs:
            if job.state != 'pending':
                if job.state == 'waiting':
                    await ctx.invoke(self.cancel, job)
                    ctx += f"{ctx.tick(False)} Cancelled #{job.id}."
                else:
                    ctx += f"{ctx.tick(False)} Can't deny #{job.id} -- it is {job.state}."
                continue
            await job.finish(ctx.author, outcome=False)
            ctx += f'{ctx.tick()} Denied #{job.id}.'

        await ctx.send_pages()

    @lifesaver.command(typing=True)
    async def approve(self, ctx, *jobs: Job):
        """Approve jobs."""
        ctx.new_paginator(prefix='', suffix='')

        for job in jobs:
            if job.state in ('finished', 'cancelled'):
                ctx += f"{ctx.tick(False)} Can't approve #{job.id} -- it is `{job.state}`"
                return
            await job.finish(ctx.author, outcome=True)
            ctx += f"{ctx.tick()} Approved #{job.id}."

        await ctx.send_pages()

    @lifesaver.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        await self.bot.wait_until_ready()

        message_id = payload.message_id

        member = self.bot.elixire.get_member(payload.user_id)
        if not member:
            return

        if (
            # only process reactions in the correct channel and guild
            payload.channel_id != self.user_registers.id
            or payload.guild_id != self.bot.elixire.id

            # only care about the right emojis
            or payload.emoji.id not in self.bot.ticks(ids=True)

            # only allow human developers
            or member.bot
            or not self.bot.is_dev(member)
        ):
            return

        async with self.pool.acquire() as conn:
            job_record = await conn.fetchrow("""
                SELECT * FROM jobs
                WHERE message_id = $1
            """, message_id)

            if not job_record:
                log.debug('Failed to find job for raw reaction event (message ID: %d).', message_id)
                return

        job = await Job.from_record(job_record, cog=self)
        is_approval = payload.emoji.id == self.bot.tick().id
        await job.finish(member, outcome=is_approval)

    @lifesaver.Cog.listener()
    async def on_member_remove(self, member):
        if member.guild != self.bot.elixire:
            return

        async with self.pool.acquire() as conn:
            record = await conn.fetchrow("""
                SELECT * FROM jobs
                WHERE (discord_tag = $1 OR discord_user_id = $2) AND state = 'pending'
            """, str(member), member.id)

        if not record:
            return

        job = await Job.from_record(record, cog=self)
        await job.prompt_ditch_denial(old_member=member)

    @lifesaver.Cog.listener()
    async def on_member_join(self, member):
        if member.guild != self.bot.elixire:
            return

        # make sure to not count members who get bounced as joining the server
        await asyncio.sleep(1)
        if self.bot.elixire.get_member(member.id) is None:
            log.debug('%s (ID: %d) was bounced.', str(member), member.id)
            return

        ditch_msg = self._ditch_prompts.get(member.id)
        if ditch_msg:
            # this user has rejoined, delete the prompt indicating that they have
            # left the server.
            try:
                await ditch_msg.delete()
            except discord.HTTPException:
                pass
            finally:
                del self._ditch_prompts[member.id]

        log.debug('%s (ID: %d) has joined.', str(member), member.id)

        await self.check_member(member)

    @lifesaver.Cog.listener()
    async def on_message(self, message):
        await self.bot.wait_until_ready()

        if message.channel != self.user_registers or not message.webhook_id:
            return

        log.debug('Job arrived from webhook %d!', message.webhook_id)

        if not message.embeds:
            log.debug('Bailing, no embeds.')
            return

        embed = message.embeds[0]

        try:
            log.info('Creating job for message %d.', message.id)
            await self.create_job(embed)
        except JobCreationError as error:
            await self.user_registers.send(f'Error while creating job: {error}')
