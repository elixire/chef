import inspect
from typing import Union

import asyncpg
import discord
import lifesaver
from lifesaver.utils import human_delta

from chef.utils import timestamp
from chef.converters import LinkedAccounts
from chef.elixire import ElixireUser


class Links(lifesaver.Cog):
    @lifesaver.command(typing=True)
    async def resolve(self, ctx, *, link: LinkedAccounts = None):
        """Resolves information about a user and their linked account.

        You may pass a reference to an elixi.re user or a Discord member. The
        other account will be resolved if found.
        """
        if link is None:
            # a bit hacky: manually call the converter with the invoker's own id
            link = await LinkedAccounts.convert(ctx, str(ctx.author))

        embed = discord.Embed()

        def format_entity(entity):
            if isinstance(entity, discord.Member):
                name = str(entity)
                mention = entity.mention
            else:
                name = entity.name
                mention = ''

            formatted = """
                {name} {mention}
                `{entity.id}`

                Created {timestamp}
                {delta} ago
            """.format(entity=entity, name=name, mention=mention,
                       timestamp=timestamp(entity.created_at),
                       delta=human_delta(entity.created_at))

            return inspect.cleandoc(formatted)

        if link.member:
            discord_field_value = format_entity(link.member)
            embed.set_thumbnail(url=link.member.avatar_url)
        else:
            discord_field_value = f'{ctx.tick(False)} Not found.'

        if link.account:
            elixire_field_value = format_entity(link.account)
        else:
            elixire_field_value = f'{ctx.tick(False)} Not found.'

        embed.color = discord.Color.green() if link.good else discord.Color.red()
        embed.add_field(name='Discord Account', value=discord_field_value)
        embed.add_field(name='elixi.re Account', value=elixire_field_value)

        await ctx.send(embed=embed)

    @lifesaver.command(typing=True)
    async def link(self, ctx, member: discord.Member, user: ElixireUser):
        """Link a Discord user to an elixi.re account."""
        try:
            async with ctx.pool.acquire() as conn:
                await conn.execute("""
                    INSERT INTO manual_links (discord_user_id, elixire_user_id)
                    VALUES ($1, $2)
                """, member.id, user.id)
        except asyncpg.IntegrityConstraintViolationError:
            await ctx.send(
                f'{ctx.tick(False)} Failed to link accounts.')
            return

        await ctx.send(f'{ctx.tick()} Linked {member} to `{user.id}`.')

    @lifesaver.command(typing=True)
    async def unlink(self, ctx, thing: Union[discord.Member, ElixireUser]):
        """Unlink a Discord user or elixi.re account from the other."""
        async with ctx.pool.acquire() as conn:
            await conn.execute("""
                DELETE FROM manual_links
                WHERE discord_user_id = $1 OR elixire_user_id = $1
            """, thing.id)

        await ctx.send(f'{ctx.tick()} Unlinked.')


def setup(bot):
    bot.add_cog(Links(bot))
