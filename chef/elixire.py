__all__ = ['Elixire']

import discord
import logging
from typing import List, Optional

import aiohttp
from discord.ext import commands

from chef.exts.approval.job import Job

log = logging.getLogger(__name__)


class ElixireError(Exception):
    def __init__(self, resp):
        super().__init__(f'HTTP {resp.status} ({resp.reason})')
        self.resp = resp


class ElixireUser:
    id: int
    name: str
    active: bool
    admin: bool
    consented: bool

    def __init__(self, data):
        self.id = int(data.get('user_id'))
        self.name = data.get('username')
        self.active = data.get('active')
        self.admin = data.get('admin')
        self.consented = data.get('consented')

        #: The :class:`discord.Member` that owns this account.
        #: This is only present when the instance is constructed from a
        #: conversion that involved a ``MemberConverter``.
        self._associated_member = None

    @property
    def created_at(self):
        return discord.utils.snowflake_time(self.id)

    @classmethod
    def from_job(cls, job: Job):
        return cls.from_record(job.record)

    @classmethod
    def from_record(cls, record):
        return cls({
            'user_id': record['user_id'],
            'username': record['username'],
        })

    @classmethod
    async def convert(cls, ctx, arg):
        # convert from a job id
        try:
            job = await Job.convert(ctx, arg)
            return cls.from_job(job)
        except commands.BadArgument:
            pass

        # convert to discord member, then find a job from that member
        try:
            member = await commands.MemberConverter().convert(ctx, arg)

            async with ctx.pool.acquire() as conn:
                record = await conn.fetchrow("""
                    SELECT user_id, username
                    FROM jobs
                    WHERE discord_tag = $1 OR discord_user_id = $2
                    AND state = 'finished'
                    AND outcome = TRUE
                """, str(member), member.id)

                if record is not None:
                    user = cls.from_record(record)
                    user._associated_member = member
                    return user
        except commands.BadArgument:
            pass

        # elixire username
        try:
            user = await ctx.bot.api.fetch_user_by_name(arg)
            if user:
                return user
        except (ElixireError, aiohttp.ClientError):
            pass

        # elixire user id
        if arg.isdigit():
            try:
                return await ctx.bot.api.fetch_user(int(arg))
            except ElixireError:
                raise commands.BadArgument(
                    'Unable to fetch a user with that ID.')

        raise commands.BadArgument(
            'Unable to resolve an elixi.re user. Try providing a job ID, '
            'Discord user mention or username, or an elixi.re user ID or username.'
        )

    def __repr__(self):
        return f'<ElixireUser id={self.id} name={self.name} admin={self.admin}>'

    def __eq__(self, other):
        return isinstance(other, ElixireUser) and other.id == self.id

    def __str__(self):
        return f'{self.name} (`{self.id}`)'


class Elixire:
    def __init__(self, bot):
        self.domain = bot.config.elixire.domain
        self.session = aiohttp.ClientSession(
            loop=bot.loop,
            headers={
                'User-Agent': 'chef',
                'Authorization': bot.config.elixire.admin_token,
            },
        )

    async def _request(self, method, endpoint, **kwargs):
        url = self.domain + endpoint
        log.debug('%s %s', method.upper(), endpoint)
        async with self.session.request(method, url, **kwargs) as r:
            if r.status >= 400:
                raise ElixireError(r)
            return await r.json()

    async def close(self):
        await self.session.close()

    async def search_users(
        self,
        *,
        active: bool = True,
        query: str = None,
        page: int = 0,
        per_page: int = 20,
    ) -> List[ElixireUser]:
        """Search for users by ID or username."""
        params = {
            'active': 'true' if active else 'false',
            'page': page,
            'per_page': per_page,
        }

        if query is not None:
            params['query'] = query

        resp = await self._request('get', '/api/admin/users/search', params=params)
        return [ElixireUser(result) for result in resp['results']]

    async def fetch_user(self, user_id: int) -> ElixireUser:
        """Fetch a user by ID. Raises if not found."""
        resp = await self._request('get', f'/api/admin/users/{user_id}')
        return ElixireUser(resp)

    async def fetch_user_by_name(self, username: str) -> Optional[ElixireUser]:
        """Fetch a user by username."""
        resp = await self.search_users(query=username, per_page=1)

        if not resp:
            return None

        if resp[0].name == username:
            return resp[0]

        return None

    async def delete_user(self, user_id: int):
        """Delete a user."""
        await self._request('delete', f'/api/admin/user/{user_id}')

    async def send_email(self, user_id: int):
        """Send an activation email to a user."""
        await self._request('post', f'/api/admin/activate_email/{user_id}')
