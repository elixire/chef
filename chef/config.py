import lifesaver


class RestrictConfig(lifesaver.config.Config):
    allowed_roles: list


class ElixireConfig(lifesaver.config.Config):
    guild_id: int
    has_account_role_id: int
    user_registers_channel_id: int
    domain: str
    admin_token: str


class MessagesConfig(lifesaver.config.Config):
    job_waiting: str
    job_pending: str
    job_cancelled: str


class ChefConfig(lifesaver.bot.BotConfig):
    elixire: ElixireConfig
    restrict: RestrictConfig
    messages: MessagesConfig
